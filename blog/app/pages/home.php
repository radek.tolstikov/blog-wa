
    <?php include '../app/pages/includes/header.php'; ?>

    <h3 class="mx-4">Nejnovější</h3>

      <div class="row containerposition-relative">

        <?php  

          $query = "select posts.*,categories.category from posts join categories on posts.category_id = categories.id order by id desc limit 6";
          $rows = query($query);
          if($rows)
          {
            foreach ($rows as $row) {
              include '../app/pages/includes/post-card.php';
            }

          }else{
            echo "Nic nebylo nalezeno!";
          }

        ?>

      </div>

      <?php include '../app/pages/includes/footer.php'; ?>
