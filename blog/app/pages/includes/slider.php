
  <!-- slider -->
  <link rel="stylesheet" href="<?=ROOT?>/assets/slider/ism/css/my-slider.css"/>
  <script src="<?=ROOT?>/assets/slider/ism/js/ism-2.2.min.js"></script>

  <div class="ism-slider" data-transition_type="fade" data-play_type="loop" id="my-slider">
    <ol>
    
    
      <li>
      <a href="<?=ROOT.'/'?>search?find=PC&počítač">
        <img src="<?=ROOT?>/assets/images/1.jpg">
        <div class="ism-caption ism-caption-0">Informace o PC buildingu</div>
        </a>
      </li>
   
      
      <li>
      <a href="<?=ROOT.'/'?>search?find=krypto">
        <img src="<?=ROOT?>/assets/images/2.jpg">
        <div class="ism-caption ism-caption-0">Uvažujete o investici do kryploměn?</div>
        </a>
      </li>
      <li>
      <a href="<?=ROOT.'/'?>search?find=VR">
        <img src="<?=ROOT?>/assets/images/3.jpg">
        <div class="ism-caption ism-caption-0">Zajímá vás virtuální realita</div>
      </li>
      </a>
      <li>
      <a href="<?=ROOT.'/'?>search?find=čas">
        <img src="<?=ROOT?>/assets/images/4.jpg">
        <div class="ism-caption ism-caption-0">Hledáte nové záliby?</div>
      </li>
      </a>
    </ol>
  </div>
  <!-- end slider -->