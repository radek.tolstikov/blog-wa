  <?php 

    //přidání nového uživatele
    if($action == 'add')
    {
        if(!empty($_POST))
        {
          //validace vloženého obsahu
          $errors = [];

          if(empty($_POST['category']))
          {
            $errors['category'] = "Je nutné vyplnit název kategorie";
          }else
          //if(!preg_match("/^[a-zA-Z0-9 \-\_\&]+$/", $_POST['category']))
          //{
          //  $errors['category'] = "Kategorie smí obsahovat pouze písmena";
         // }

          $slug = str_to_url($_POST['category']);

          $query = "select id from categories where slug = :slug limit 1";
          $slug_row = query($query, ['slug'=>$slug]);
 
          if($slug_row)
          {
            $slug .= rand(1000,9999);
          }
   
          if(empty($errors))
          {
            //uložení do databáze
            $data = [];
            $data['category'] = $_POST['category'];
            $data['slug']     = $slug;
            $data['disabled'] = $_POST['disabled'];

            $query = "insert into categories (category,slug,disabled) values (:category,:slug,:disabled)";
            query($query, $data);

            redirect('admin/categories');

          }
        }
    }else
    if($action == 'edit')
    {
        
        $query = "select * from categories where id = :id limit 1";
        $row = query_row($query, ['id'=>$id]);

        if(!empty($_POST))
        {

          if($row)
          {

            //validace vložených dat
            $errors = [];

            if(empty($_POST['category']))
            {
              $errors['category'] = "Je nutné vyplnit název kategorie";
            }else
           // if(!preg_match("/^[a-zA-Z0-9 \-\_\&]+$/", $_POST['category']))
           // {
            //  $errors['category'] = "Kategorie smí obsahovat pouze písmena";
           // }
     
            if(empty($errors))
            {
              
              $data = [];
              $data['category'] = $_POST['category'];
              $data['disabled'] = $_POST['disabled'];
              $data['id'] = $id;

              $query = "update categories set category = :category, disabled = :disabled where id = :id limit 1";

              query($query, $data);
              redirect('admin/categories');

            }
          }
        }
    }else
    if($action == 'delete')
    {
        
        $query = "select * from categories where id = :id limit 1";
        $row = query_row($query, ['id'=>$id]);

        if($_SERVER['REQUEST_METHOD'] == "POST")
        {

          if($row)
          {

          
            $errors = [];
 
            if(empty($errors))
            {
              
              $data = [];
              $data['id']       = $id;

              $query = "delete from categories where id = :id limit 1";
              query($query, $data);
 
              redirect('admin/categories');

            }
          }
        }
      }